function form_send(form) {
	if (!isset(form.statusMsg)) form.statusMsg = null;
	var _FormValuesGetter = new FormValuesGetter(form),
		values = _FormValuesGetter.getValues();
	if (!isset(values.formName)) {
		if (isset(values.s)) {
			window.location.href = '/?s=' + values.s;
		}
		return false;
	}
	var _Request = new Request({
		method: 'formsSubmit',
		model: values
	},
		{
			before: function () {
				if (form.statusMsg) {
					form.removeChild(form.statusMsg);
					form.statusMsg = null;
				}
				form.style.opacity = 0.5;
			},
			after: function () {
				form.style.opacity = 1;
			},
			success: function (model) {
				statusMsg({
					model: model,
					form: form,
					is_ok: true
				});
				form_clear(form);
				setTimeout(function () {
					popup_close();
				}, 5000);
				if (isset(model.newWindow)) {
					win = window.open(model.newWindow, '_blank');
					if (win) {
						win.focus();
					} else {
						window.location.href = model.newWindow;
					}
				}
				if (isset(model.windowRedirect)) {
					window.location.href = model.windowRedirect;
				}
			},
			error: function (model) {
				statusMsg({
					model: model,
					form: form,
					is_ok: false
				});
			}
		});
	_Request.start();
}

var emptyFunction = function () { },
	isset = function (v) { return typeof v !== 'undefined'; },
	is_function = function (v) { return (typeof v === 'function'); },
	is_array = function (v) { return (typeof v === 'object' && v instanceof Array && isset(v.length)); },
	is_object = function (v) { return (typeof v === 'object' && v instanceof Object && !(v instanceof Array)); },
	is_numeric = function (v) { return !isNaN(v); },
	is_NodeList = function (v) { return typeof v === 'object' && v instanceof Object && v instanceof NodeList; },
	is_Node = function (v) { return typeof v === 'object' && v instanceof Object && v instanceof Node; },
	is_string = function (v) { return (typeof v === 'string'); },
	is_iOS = function () { return (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream); },
	is_bool = function (v) { return (typeof v === 'boolean') && (v === true || v === false); },
	is_empty = function (v) { return (v === ''); },
	is_not_empty = function (v) { return !is_empty(v); },
	Request = function (model, callbacks) {
		var self = this;
		model = (isset(model) ? model : {});
		this.model = {
			method: (isset(model.method) ? model.method : null),
			model: (isset(model.model) ? model.model : {}),
		};
		callbacks = (isset(callbacks) ? callbacks : {});
		this.callbacks = {
			before: (isset(callbacks.before) ? callbacks.before : emptyFunction),
			after: (isset(callbacks.after) ? callbacks.after : emptyFunction),
			success: (isset(callbacks.success) ? callbacks.success : emptyFunction),
			error: (isset(callbacks.error) ? callbacks.error : emptyFunction),
			state: (isset(callbacks.state) ? callbacks.state : emptyFunction)
		};
		var xhr = null,
			response = null;
		this.started = false;
		this.finished = false;
		this.getResponse = function () { return response ? Object.assign(response, {}) : null; };
		this.start = function () {
			self.stop();
			self.callbacks.before();
			self.started = true;
			xhr = new XMLHttpRequest();
			xhr.open('POST', '/wp-admin/admin-ajax.php?action=ThemeHelpers', true);
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.send(['method=', self.model.method, '&model=', encodeURIComponent(JSON.stringify(self.model.model))].join(''));
			xhr.onreadystatechange = function () {
				self.callbacks.state();
				if (this.readyState !== 4) return;
				if (this.status == 200) {
					self.finished = true;
					response = JSON.parse(this.responseText);
					if (response && isset(response.status) && response.status) {
						self.callbacks.success(response);
					} else {
						self.callbacks.error(response);
					}
				} else {
					self.callbacks.error();
				}
				self.callbacks.after();
			};
		};
		this.stop = function () {
			if (self.is_started() && !self.is_finished()) {
				xhr.abord();
				xhr = null;
			}
		};
		this.is_started = function () {
			return self.started;
		};
		this.is_finished = function () {
			return self.finished;
		};
	},
	FormValuesGetter = function (form) {
		var self = this;
		var _inputs = form.querySelectorAll('input[name], select[name], textarea[name]');
		this.inputs = {};
		var _inp;
		for (var i = _inputs.length - 1; i >= 0; i--) {
			_inp = _inputs[i].getAttribute('name');
			this.inputs[_inp] = _inputs[i];
		}
		this.getValues = function () {
			var model = {}, val;
			for (var name in self.inputs) {
				val = self.getInputValue(name);
				if (val !== null) model[name] = val;
			}
			return model;
		};
		this.getInputValue = function (name) {
			if (!isset(self.inputs[name])) return null;
			var value = self.inputs[name].value,
				tag = self.inputs[name].tagName.toLowerCase(),
				type = self.inputs[name].getAttribute('type');
			switch (tag) {
				case 'select':
				case 'textarea':
					return self.inputs[name].value;
					break;
				case 'input':
					switch (type) {
						case 'checkbox':
						case 'radio':
							return self.inputs[name].checked ? self.inputs[name].value : null;
							break;
						default:
							return self.inputs[name].value;
							break;
					}
					break;
				default:
					return null;
					break;
			}
		};
	},
	statusMsg = function (model) {
		model.form.statusMsg = document.createElement('div');
		model.form.statusMsg.setAttribute('style', ['padding:20px 30px;margin:20px 0;border-radius:3px;border:1px solid ', (model.is_ok ? 'green' : 'red'), ';text-align:center;color:', (model.is_ok ? 'green' : 'red'), ';font-weight:700'].join(''));
		if (isset(model.model) && isset(model.model.statusMsg)) {
			model.form.statusMsg.innerText = model.model.statusMsg;
		} else {
			model.form.statusMsg.innerText = (model.is_ok ? 'Спасибо!' : 'Произошла ошибка, попробуйте позже.');
		}
		model.form.appendChild(model.form.statusMsg);
	};

/*document.addEventListener('DOMContentLoaded', function(){
  var playloadPostContentInner = function(inner) {
    var self = this;
    var i = null,
        tableOfContents = inner.querySelectorAll('.wp-block-uagb-table-of-contents');
    if (tableOfContents&&tableOfContents.length) {
      for (i = tableOfContents.length - 1; i >= 0; i--) {
        defenceTableOfContents(tableOfContents[i]);
      }
    }
  },
  defenceTableOfContents = function(item) {
    setTimeout(function(){
      addMultipleClasses(item, ['elements__content','content']);
      var wrap = item.querySelector('.uagb-toc__wrap');
      if (wrap) wrap.className = 'content__body';
      var title = item.querySelector('.uagb-toc__title-wrap');
      if (title) {
        title.parentNode.removeChild(title);
        item.innerHTML = '<h2>'+title.innerText+'</h2>'+item.innerHTML;
      };
      wrap = item.querySelector('.uagb-toc__list-wrap');
      if (wrap) {
        var headers = JSON.parse(wrap.dataset.headers);
        var html = ['<ul class="content__list">'];
        for (var i=0; i<headers.length; i++) {
          html.push(['<li class="content__element">',
                  '<a href="#',headers[i].link,'" class="content__link">',
                    '<span>',headers[i].text,'</span>',
                  '</a>',
                  '<a href="#',headers[i].link,'" class="content__number">',(i+1),'</a>',
                '</li>'].join(''));
        }
        html.push('</ul>');
        var parent = wrap.parentNode;
        parent.removeChild(wrap);
        parent.innerHTML = html.join('');
      }
    }, 500);
  },
  addMultipleClasses = function(item, array) {
    for (var i = array.length - 1; i >= 0; i--) {
      item.classList.add(array[i]);
    }
  };
  var post_content_inners = document.querySelectorAll('.post_content_inner');
  for (var i = post_content_inners.length - 1; i >= 0; i--) {
    playloadPostContentInner(post_content_inners[i]);
  }
});*/