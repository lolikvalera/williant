<?php

/**
 * Template Name: Contacts Template
 */

get_header();

$template_url	= get_bloginfo('template_url');

if (have_posts()) :
  while (have_posts()) :
    the_post();

?>
<main class="page">
	<div class="page__contact contact _container">
		<div class="contact__row">
			<div class="contact__column">
				<div class="contact__item item-contact">
					<div class="item-contact__title">Контакты </div>
					<div class="item-contact__subtitle">
						<p><?php echo Contacts::get('address'); ?></p>
					</div>

					<div class="item-contact__info info-contact">
						<div class="info-contact__body">
							<div class="info-contact__row">
								<div class="info-contact__column">
									<picture><source srcset="<?=$template_url?>/img/icons/icon_email_light_blue.svg" type="image/webp"><img src="<?=$template_url?>/img/icons/icon_email_light_blue.svg" alt=""></picture>
								</div>
								<div class="info-contact__column">
									<a href="mailto:<?php echo Contacts::get('email1'); ?>" class="info-contact__mail"><?php echo Contacts::get('email1'); ?></a>
								</div>
							</div>
							<div class="info-contact__row">
								<div class="info-contact__column">
									<picture><source srcset="<?=$template_url?>/img/icons/phone_light_blue.svg" type="image/webp"><img src="<?=$template_url?>/img/icons/phone_light_blue.svg" alt=""></picture>
								</div>
								<div class="info-contact__column">
									<a href="tel:<?php echo Contacts::get('phone1'); ?>" class="info-contact__phone phone"><?php echo Contacts::get('phone1'); ?></a>
								</div>
							</div>
							<div class="info-contact__row">
								<div class="info-contact__column">
									<picture><source srcset="<?=$template_url?>/img/icons/icon_fax.svg" type="image/webp"><img src="<?=$template_url?>/img/icons/icon_fax.svg" alt=""></picture>
								</div>
								<div class="info-contact__column">
									<a href="tel:<?php echo Contacts::get('phone2'); ?>" class="info-contact__phone phone"><?php echo Contacts::get('phone2'); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="contact__column">
				<div id="map" class="contact__map">
					
				</div>
			</div>
		</div>
	</div>
	<div class="page__feedback feedback">
		<div class='_container'>
			<div class="feedback__body body-feedback">
				<div class="body-feedback__title title-catalog"><span class="title-catalog__red">Напишите нам</span></div>
				<form action="#" class="body-feedback__form form-feedback">
					<input type="hidden" name="formName" value="feedBack">
					<div class="form-feedback__input">
						<input autocomplete="off" type="text" name="name" data-error="Ошибка" data-value="Имя / Организация" class="input _req" />
					</div>
					<div class="form-feedback__row">
						<div class="form-feedback__column">
							<div class="form-feedback__input">
								<input autocomplete="off" type="text" name="email" data-error="Ошибка" data-value="Электронная почта" class="input _email _req" />
							</div>
						</div>
						<div class="form-feedback__column">
							<div class="form-feedback__input">
								<input autocomplete="off" type="text" name="phone" data-error="Ошибка" data-value="Телефон" class="input _phone _req" />
							</div>
						</div>
					</div>
					<div class="form-feedback__text-area">
						<textarea name="msg" rows="5" data-error="Ошибка" class="input _req" data-value="Текст сообщения"></textarea>
					</div>
					<div class="form-feedback__button">
						<button type="submit" class="form-feedback__btn btn-send">Отправить <span>сообщение</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</main>
<script src="https://maps.google.com/maps/api/js?sensor=false&amp;key=AIzaSyD3iLp3V9nfI6F5RdnqJC2nlRR23ciyZiM"></script>
<?php
  endwhile;
endif;

get_footer();

?>