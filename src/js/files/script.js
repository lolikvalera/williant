let searh_icon = document.querySelector(".item-body-header__search");
searh_icon.addEventListener("click", function (e) {
	document.querySelector('.item-body-header__form').classList.add("_active");
	e.preventDefault();
})

let serach_close = document.querySelector(".form-block__closed");
serach_close.addEventListener("click", function (e) {
	document.querySelector('.item-body-header__form').classList.remove("_active");
	e.preventDefault();
})

let menu_parent = document.querySelector('.menu__parent');
if (window.innerWidth > 1160) {
	menu_parent.addEventListener("mouseenter", function (e) {
		menu_parent.classList.add('hover');
	});
	menu_parent.addEventListener("mouseleave", function (e) {
		menu_parent.classList.remove('hover');
	});
} else {
	let link_parent = document.querySelector('.menu__link_parent');
	link_parent.addEventListener("click", function (e) {
		menu_parent.classList.toggle('active');
		e.preventDefault();
	});
}
let tables = document.querySelectorAll('.content table');
if (tables.length > 0) {
	for (let index = 0; index < tables.length; index++) {
		const table = tables[index];
		let table_wrapper = document.createElement('div');
		table_wrapper.classList.add('tables__block');
		wrap(table, table_wrapper);

		let th = table.querySelectorAll('th');
		for (let index = 0; index < th.length; index++) {
			const el = th[index];
			if (!el.querySelector('span')) {
				let span = document.createElement('span');
				span.innerHTML = el.innerHTML;
				el.innerHTML = '';
				el.appendChild(span);
			}
		}
		let td = table.querySelectorAll('td');
		for (let index = 0; index < td.length; index++) {
			const el = td[index];
			if (!el.querySelector('span')) {
				let span = document.createElement('span');
				span.innerHTML = el.innerHTML;
				el.innerHTML = '';
				el.appendChild(span);
			}
		}
	}
}

let link_goto = document.querySelectorAll('.uagb-toc__list a');
if (link_goto.length > 0) {
	for (let index = 0; index < link_goto.length; index++) {
		const el = link_goto[index];
		el.addEventListener("click", function (e) {
			let obj_name = el.getAttribute('href').replace('#', '');
			let obj = document.getElementById(obj_name);
			//console.log(obj);
			goto(obj, 300);
			e.preventDefault();
		});
	}
}
