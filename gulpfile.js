//let replace = require('gulp-replace'); //.pipe(replace('bar', 'foo'))
let { src, dest } = require("gulp");
let fs = require('fs');
let gulp = require("gulp");
let browsersync = require("browser-sync").create();
let autoprefixer = require("gulp-autoprefixer");
let scss = require("gulp-sass");
let group_media = require("gulp-group-css-media-queries");
let plumber = require("gulp-plumber");
let del = require("del");
let imagemin = require("gulp-imagemin");
let uglify = require("gulp-uglify-es").default;
let rename = require("gulp-rename");
let fileinclude = require("gulp-file-include");
let clean_css = require("gulp-clean-css");

let cache = require("gulp-cache");

let babel = require('gulp-babel');

let webp = require('gulp-webp');
let webpcss = require("gulp-webpcss");
let webphtml = require('gulp-webp-html');

let fonter = require('gulp-fonter');

let ttf2woff = require('gulp-ttf2woff');
let ttf2woff2 = require('gulp-ttf2woff2');

let iconfontCss = require('gulp-iconfont-css');

let project_name = require("path").basename(__dirname);

let path = {
	build: {
		html: project_name + "/",
		js: project_name + "/js/",
		css: project_name + "/css/",
		images: project_name + "/img/",
		fonts: project_name + "/fonts/"
	},
	src: {
		favicon: "src/img/favicon.{jpg,png,svg,gif,ico,webp}",
		html: ["src/*.html", "!src/_*.html"],
		js: ["src/js/app.js", "src/js/vendors.js"],
		css: "src/scss/style.scss",
		images: ["src/img/**/*.{jpg,png,svg,gif,ico,webp}", "!**/favicon.*"],
		iconfonts: "src/iconfont/src/*.svg",
		fonts: "src/fonts/*.ttf"
	},
	watch: {
		html: "src/**/*.html",
		js: "src/**/*.js",
		css: "src/scss/**/*.scss",
		images: "src/img/**/*.{jpg,png,svg,gif,ico,webp}",
		iconfonts: "src/iconfont/**/*.svg",
		fonts: "src/fonts/**/*.ttf"
	},
	clean: "./" + project_name + "/"
};
function browserSync(done) {
	browsersync.init({
		server: {
			baseDir: "./" + project_name + "/"
		},
		notify: false,
		port: 3000
	});
}
function html() {
	return src(path.src.html, { base: "src/" })
		.pipe(plumber())
		.pipe(fileinclude())
		.pipe(webphtml())
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream());
}
function css() {
	return src(path.src.css, { base: "src/scss/" })
		.pipe(plumber())
		.pipe(
			scss({
				outputStyle: "expanded"
			})
		)
		.pipe(group_media())
		.pipe(
			autoprefixer({
				overrideBrowserslist: ["last 5 versions"],
				cascade: true
			})
		)
		.pipe(webpcss())
		.pipe(dest(path.build.css))
		.pipe(clean_css())
		.pipe(
			rename({
				suffix: ".min",
				extname: ".css"
			})
		)
		.pipe(dest(path.build.css))
		.pipe(browsersync.stream());
}
function js() {
	return src(path.src.js, { base: "src/js/" })
		.pipe(plumber())
		.pipe(fileinclude())
		.pipe(babel({
			presets: [["@babel/env"]]
		}))
		.pipe(gulp.dest(path.build.js))
		.pipe(uglify(/* options */))
		.pipe(
			rename({
				suffix: ".min",
				extname: ".js"
			})
		)
		.pipe(dest(path.build.js))
		.pipe(browsersync.stream());
}
function images() {
	return src(path.src.images)
		.pipe(plumber())
		.pipe(cache(webp({
			quality: 80
		})))
		.pipe(dest(path.build.images))
		.pipe(src(path.src.images))
		.pipe(
			imagemin({
				progressive: true,
				svgoPlugins: [{ removeViewBox: false }],
				interlaced: true,
				optimizationLevel: 3 // 0 to 7
			})
		)
		.pipe(dest(path.build.images))
}
function favicon() {
	return src(path.src.favicon)
		.pipe(plumber())
		.pipe(
			rename({
				//suffix: ".min",
				extname: ".ico"
			})
		)
		.pipe(dest(path.build.html))
}
function fonts_otf() {
	return src('./src/fonts/*.otf')
		.pipe(plumber())
		.pipe(fonter({
			//deflate:,
			//inflate:,
			formats: ['ttf']
		}))
		.pipe(gulp.dest('./src/fonts/'));
}
function fonts() {
	src(path.src.fonts)
		.pipe(plumber())
		.pipe(ttf2woff())
		.pipe(dest(path.build.fonts));
	return src(path.src.fonts)
		.pipe(ttf2woff2())
		.pipe(dest(path.build.fonts))
		.pipe(browsersync.stream());
}
function fontstyle() {
	let file_content = fs.readFileSync('src/scss/fonts.scss');
	if (file_content == '') {
		fs.writeFile('src/scss/fonts.scss', '', cb);
		return fs.readdir(path.build.fonts, function (err, items) {
			if (items) {
				let c_fontname;
				for (var i = 0; i < items.length; i++) {
					let fontname = items[i].split('.');
					fontname = fontname[0];
					if (c_fontname != fontname) {
						fs.appendFile('src/scss/fonts.scss', '@include font("' + fontname + '", "' + fontname + '", "400", "normal");\r\n', cb);
					}
					c_fontname = fontname;
				}
			}
		})
	}
}
function infofile() {
	let info = 'info.txt';
	if (!fs.existsSync(info)) {
		fs.writeFile(info, '', cb);
		fs.appendFile(info, 'http://andrikanich.guru/freelance/2020/' + project_name + '\r\n', cb);
		fs.appendFile(info, 'http://andrikanich.guru/freelance/2020/' + project_name + '/' + project_name + '.zip\r\n', cb);
		fs.appendFile(info, '================================================================\r\n', cb);
	}
}

// gulp ic (создать иконочный шрифт)
gulp.task('ic', function () {
	src(path.src.iconfonts)
		.pipe(plumber())
		.pipe(iconfontCss({
			fontName: 'icons',
			path: 'src/scss/icons-template.scss',
			targetPath: '../icons.scss',
			fontPath: '../fonts/'
		}))
});

function cb() { }
function clean() {
	if (!fs.existsSync('info.txt') && project_name != 'fls_start_gulp') {
		del('./.git/');
		fs.writeFile('./.gitignore', '', cb);
		fs.appendFile('./.gitignore', 'node_modules/', cb);
	}
	return del(path.clean);
}
function watchFiles() {
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.js], js);
	gulp.watch([path.watch.images], images);
	gulp.watch([path.watch.fonts], fonts);
}
let build = gulp.series(clean, fonts_otf, gulp.parallel(html, css, js, favicon, images), fonts, gulp.parallel(fontstyle, infofile));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.html = html;
exports.css = css;
exports.js = js;
exports.favicon = favicon;
exports.infofile = infofile;
exports.fonts_otf = fonts_otf;
exports.fontstyle = fontstyle;
exports.fonts = fonts;
exports.images = images;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = watch;