//Placeholers
let inputs = document.querySelectorAll('input[data-value],textarea[data-value]');
if (inputs) {
	for (let index = 0; index < inputs.length; index++) {
		const input = inputs[index];
		const input_g_value = input.getAttribute('data-value');
		if (input.value == '' && input_g_value != '') {
			input.value = input_g_value;
		}
		if (input.value != '' && input.value != input_g_value) {
			input_focus_add(input);
		}
		input.addEventListener('focus', function (e) {
			if (input.value == input_g_value) {
				input_focus_add(input);
				input.value = '';
			}
			if (input.classList.contains('_phone')) {
				//'+7(999) 999 9999'
				//'+38(999) 999 9999'
				//'+375(99)999-99-99'
				Inputmask("+7(999) 999 9999", {
					//"placeholder": '',
					clearIncomplete: true,
					clearMaskOnLostFocus: true,
					onincomplete: function () {
						input.inputmask.remove();
						input.value = input_g_value;
						input_focus_remove(input);
					}
				}).mask(input);
			}
			form_remove_error(input);
		});
		input.addEventListener('blur', function (e) {
			if (input.value == '') {
				if (input.classList.contains('_phone')) {
					if (input.inputmask) {
						input.inputmask.remove();
					}
				}
				input.value = input_g_value;
				input_focus_remove(input);
			}
		});
		if (input.classList.contains('_date')) {
			const datepicker = new Datepicker(input, {
				language: 'ru',
				autohide: true,
				showDaysOfWeek: false,
				format: 'dd-mm-yyyy',
			});
		}
	}
}
function input_focus_add(input) {
	input.classList.add('focus');
	input.parentElement.classList.add('focus');
}
function input_focus_remove(input) {
	input.classList.remove('focus');
	input.parentElement.classList.remove('focus');
}

let btn = document.querySelectorAll('button[type="submit"],input[type="submit"]');
if (btn) {
	for (let index = 0; index < btn.length; index++) {
		const el = btn[index];
		el.addEventListener('click', form_submit);
	}
}
function form_submit() {
	let error = 0;
	let btn = event.target;
	let form = btn.closest('form');
	let form_req = form.querySelectorAll('._req');

	if (form_req) {
		for (let index = 0; index < form_req.length; index++) {
			const el = form_req[index];
			error += form_validate(el);
		}
	}
	if (error == 0) {
		//SendForm
		form_send(form);
		//popup_close();
		//popup_open('message');
		event.preventDefault();
	} else {
		let form_error = form.querySelectorAll('._error');
		if (form_error && form.classList.contains('_goto-error')) {
			goto(form_error[0], 1000, 50);
		}
		event.preventDefault();
	}
}
function form_validate(input) {
	let error = 0;
	let input_g_value = input.getAttribute('data-value');

	if (input.getAttribute("name") == "email" || input.classList.contains("_email")) {
		if (input.value != input_g_value) {
			let em = input.value.replace(" ", "");
			input.value = em;
		}
		if (email_test(input) || input.value == input_g_value) {
			form_add_error(input);
			error++;
		} else {
			form_remove_error(input);
		}
	} else {
		if (input.value == '' || input.value == input_g_value) {
			form_add_error(input);
			error++;
		} else {
			form_remove_error(input);
		}
	}
	return error;
}
function form_clear(form) {
	let inputs = form.querySelectorAll('input,textarea');
	for (let index = 0; index < inputs.length; index++) {
		const input = inputs[index];
		input.value = '';
		input.value = input.getAttribute('data-value');
	}
}
function form_add_error(input) {
	input.classList.add('_error');
	input.parentElement.classList.add('_error');

	let input_error = input.parentElement.querySelector('.form__error');
	if (input_error) {
		input.parentElement.removeChild(input_error);
	}
	let input_error_text = input.getAttribute('data-error');
	if (input_error_text && input_error_text != '') {
		input.parentElement.insertAdjacentHTML('beforeend', '<div class="form__error">' + input_error_text + '</div>');
	}
}
function form_remove_error(input) {
	input.classList.remove('_error');
	input.parentElement.classList.remove('_error');

	let input_error = input.parentElement.querySelector('.form__error');
	if (input_error) {
		input.parentElement.removeChild(input_error);
	}
}


//Select
function selects_init() {
	let selects = document.querySelectorAll('select');
	if (selects) {
		for (let index = 0; index < selects.length; index++) {
			const select = selects[index];
			select_init(select);
		}
		//select_callback();
	}
}
function select_init(select) {
	const select_parent = select.parentElement;
	const select_modifikator = select.getAttribute('class');
	select.style.display = 'none';

	select_parent.insertAdjacentHTML('beforeend', '<div class="select select_' + select_modifikator + '"></div>');

	let new_select = select.parentElement.querySelector('.select');
	new_select.append(select);
	select_item(select);
}
function select_item(select) {
	const select_parent = select.parentElement;
	const select_items = select_parent.querySelector('.select__item');
	const select_options = select.querySelectorAll('option');
	const select_selected_option = select.querySelector('option:checked');
	const select_selected_text = select_selected_option.text;
	const select_type = select.getAttribute('data-type');

	if (select_items) {
		select_items.remove();
	}

	let select_type_content = '';
	if (select_type == 'input') {
		select_type_content = '<div class="select__value icon-select-arrow"><input autocomplete="off" type="text" name="form[]" value="' + select_selected_text + '" data-error="Ошибка" data-value="' + select_selected_text + '" class="select__input"></div>';
	} else {
		select_type_content = '<div class="select__value icon-select-arrow">' + select_selected_text + '</div>';
	}

	select_parent.insertAdjacentHTML('beforeend',
		'<div class="select__item">' +
		'<div class="select__title">' + select_type_content + '</div>' +
		'<div class="select__options">' + select_get_options(select_options) + '</div>' +
		'</div></div>');

	select_actions(select, select_parent);
}
function select_actions(original, select) {
	const select_item = select.querySelector('.select__item');
	const select_options = select.querySelectorAll('.select__option');
	const select_type = original.getAttribute('data-type');
	const select_input = select.querySelector('.select__input');

	select_item.addEventListener('click', function () {
		select.classList.toggle('_active');
	});

	for (let index = 0; index < select_options.length; index++) {
		const select_option = select_options[index];
		const select_option_value = select_option.getAttribute('data-value');
		const select_option_text = select_option.innerHTML;

		if (select_type != 'input') {
			if (select_option.getAttribute('data-value') == original.value) {
				select_option.style.display = 'none';
			}
		}
		select_option.addEventListener('click', function () {
			for (let index = 0; index < select_options.length; index++) {
				const el = select_options[index];
				el.style.display = 'block';
			}

			if (select_type == 'input') {
				select_input.value = select_option_text;
				original.value = select_option_value;
			} else {
				select.querySelector('.select__value').innerHTML = select_option_text;
				original.value = select_option_value;
				select_option.style.display = 'none';
			}
		});
	}
}
function select_get_options(select_options) {
	if (select_options) {
		let select_options_content = '';
		for (let index = 0; index < select_options.length; index++) {
			const select_option = select_options[index];
			const select_option_value = select_option.value;
			if (select_option_value != '') {
				const select_option_text = select_option.text;
				select_options_content = select_options_content + '<div data-value="' + select_option_value + '" class="select__option">' + select_option_text + '</div>';
			}
		}
		return select_options_content;
	}
}
function selects_update_all() {
	let selects = document.querySelectorAll('select');
	if (selects) {
		for (let index = 0; index < selects.length; index++) {
			const select = selects[index];
			select_item(select);
		}
	}
}
selects_init();

