<?php

get_header();

$template_url	= get_bloginfo('template_url');

?>
<main class="page page_inner">
	<div class="page__elements elements">
		<div class="elements__container _container">
			<div class="elements__header header-elements">
				<div class="header-elements__row">
					<div class="header-elements__column">
						<span>Поиск</span>
					</div>
				</div>
			</div>
			<div class="elements__content content">
				<div class="elements__start start">
					<h1>Поиск по запросу: <?php echo $_GET['s']; ?></h1>
					<div class="search">
						<?php
						if (have_posts()) :
						  while (have_posts()) :
							the_post();
							?>
							<div class="search__item item-search">
								<a href="<? echo get_the_permalink(); ?>" class="item-search__title">
									<? echo get_the_title(); ?>
								</a>
								<div class="item-search__text text">
									<? echo get_the_excerpt(); ?>
								</div>
							</div>
						    <?php
						  endwhile;
						else:
						  echo 'Ничего не найдено';
						endif;
						?>
						<!-- 
							<?
								$post_model = [
								'title' => get_the_title(),
								'url' => get_the_permalink(),
								'image' => get_the_post_thumbnail_url(),
								'excerpt' => get_the_excerpt(),
								];
							?>
							<pre><?php// var_dump($post_model); ?></pre>
						 -->
					</div>
				</div>
			</div>
		</div>
		<div class="elements__conect conect-elements">
			<div class='_container'>
				<div class="conect-elements__body">
					<div class="conect-elements__row">
						<div class="conect-elements__column">
							<div class="conect-elements__item item-column">
								<div class="item-column__title title-catalog">
									<p class="title-catalog__text"><span class="title-catalog__red">Получить каталог</span> продукции <span>Willbrandt</span></p>
								</div>
								<form action="#" class="item-column__form form-catalog">
									<input type="hidden" name="formName" value="catalogRequest">
									<div class="form-catalog__input">
										<input autocomplete="off" type="text" name="name" data-value="Имя / Организация" data-error="Ошибка" class="input _req" />
									</div>
									<div class="form-catalog__input">
										<input autocomplete="off" type="text" name="email" data-value="Электронная почта" data-error="Ошибка" class="input _email _req" />
									</div>
									<button type="submit" class="form-catalog__btn btn-catalog"><span>Скачать каталог</span></button>
								</form>
							</div>
						</div>
						<div class="conect-elements__column">
							<div class="conect-elements__item item-column">
								<div class="item-column__title title-catalog">
									<p class="title-catalog__text"><span class="title-catalog__red">Связаться</span> с нашими специалистами</p>
								</div>
								<div class="item-column__subtitle">Мы с готовностью ответим на ваши вопросы.</div>
								<a href="tel:<?php echo Contacts::get('phone1'); ?>" class="item-column__phone"><?php echo Contacts::get('phone1'); ?></a>
								<div class="item-column__subtitle">понедельник-пятница, с 9:00 до 18:00 по московскому времени</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php

get_footer();

?>