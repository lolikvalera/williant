"use strict";

function email_test(input) {
  return !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(input.value);
}

var ua = window.navigator.userAgent;
var msie = ua.indexOf("MSIE ");
var isMobile = {
  Android: function Android() {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function BlackBerry() {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function iOS() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function Opera() {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function Windows() {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: function any() {
    return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
  }
};

function isIE() {
  ua = navigator.userAgent;
  var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
  return is_ie;
}

if (isIE()) {
  document.querySelector('body').classList.add('ie');
}

if (isMobile.any()) {
  document.querySelector('body').classList.add('touch');
}

function testWebP(callback) {
  var webP = new Image();

  webP.onload = webP.onerror = function () {
    callback(webP.height == 2);
  };

  webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}

testWebP(function (support) {
  if (support == true) {
    document.querySelector('body').classList.add('webp');
  }
});

function ibg() {
  if (isIE()) {
    var _ibg = document.querySelectorAll("._ibg");

    for (var i = 0; i < _ibg.length; i++) {
      if (_ibg[i].querySelector('img') && _ibg[i].querySelector('img').getAttribute('src') != null) {
        _ibg[i].style.backgroundImage = 'url(' + _ibg[i].querySelector('img').getAttribute('src') + ')';
      }
    }
  }
}

ibg();

if (document.querySelector('.wrapper')) {
  document.querySelector('.wrapper').classList.add('_loaded');
} //ActionsOnHash

/*
if (location.hash) {
	var hsh = location.hash.replace('#', '');
	if (document.querySelector('.popup_' + hsh)) {
		popup_open(hsh);
	} else if (document.querySelector('div.' + hsh)) {
		goto(document.querySelector('.' + hsh), 500, '');
	}
}
*/
//Menu


var iconMenu = document.querySelector(".icon-menu");

if (iconMenu != null) {
  var delay = 500;
  var body = document.querySelector("body");
  var menuBody = document.querySelector(".menu__body");
  iconMenu.addEventListener("click", function (e) {
    if (!body.classList.contains('_wait')) {
      if (window.innerWidth < 768) {
        body_lock(delay);
      }

      iconMenu.classList.toggle("_active");
      menuBody.classList.toggle("_active");
    }
  });
}

;

function menu_close() {
  var iconMenu = document.querySelector(".icon-menu");
  var menuBody = document.querySelector(".menu__body");
  iconMenu.classList.remove("_active");
  menuBody.classList.remove("_active");
}

function body_lock(delay) {
  var body = document.querySelector("body");

  if (body.classList.contains('_lock')) {
    body_lock_remove(delay);
  } else {
    body_lock_add(delay);
  }
}

function body_lock_remove(delay) {
  var body = document.querySelector("body");

  if (!body.classList.contains('_wait')) {
    var lock_padding = document.querySelectorAll("._lp");
    setTimeout(function () {
      for (var index = 0; index < lock_padding.length; index++) {
        var el = lock_padding[index];
        el.style.paddingRight = '0px';
      }

      body.style.paddingRight = '0px';
      body.classList.remove("_lock");
    }, delay);
    body.classList.add("_wait");
    setTimeout(function () {
      body.classList.remove("_wait");
    }, delay);
  }
}

function body_lock_add(delay) {
  var body = document.querySelector("body");

  if (!body.classList.contains('_wait')) {
    var lock_padding = document.querySelectorAll("._lp");

    for (var index = 0; index < lock_padding.length; index++) {
      var el = lock_padding[index];
      el.style.paddingRight = window.innerWidth - document.querySelector('.wrapper').offsetWidth + 'px';
    }

    body.style.paddingRight = window.innerWidth - document.querySelector('.wrapper').offsetWidth + 'px';
    body.classList.add("_lock");
    body.classList.add("_wait");
    setTimeout(function () {
      body.classList.remove("_wait");
    }, delay);
  }
} //Tabs


var tabs = document.querySelectorAll("._tabs");

var _loop = function _loop(index) {
  var tab = tabs[index];
  var tabs_items = tab.querySelectorAll("._tabs-item");
  var tabs_blocks = tab.querySelectorAll("._tabs-block");

  var _loop9 = function _loop9(_index29) {
    var tabs_item = tabs_items[_index29];
    tabs_item.addEventListener("click", function (e) {
      for (var _index30 = 0; _index30 < tabs_items.length; _index30++) {
        var _tabs_item = tabs_items[_index30];

        _tabs_item.classList.remove('_active');
      }

      for (var _index31 = 0; _index31 < tabs_blocks.length; _index31++) {
        var tabs_block = tabs_blocks[_index31];
        tabs_block.classList.remove('_active');
      }

      tabs_item.classList.add('_active');

      tabs_blocks[_index29].classList.add('_active');
    });
  };

  for (var _index29 = 0; _index29 < tabs_items.length; _index29++) {
    _loop9(_index29);
  }
};

for (var index = 0; index < tabs.length; index++) {
  _loop(index);
} //Gallery


var gallery = document.querySelectorAll('.wp-block-image');

if (gallery) {
  gallery_init();
}

function gallery_init() {
  for (var _index = 0; _index < gallery.length; _index++) {
    var el = gallery[_index];
    lightGallery(el, {
      counter: false,
      selector: 'a',
      download: false
    });
  }
} //SearchInList


function search_in_list(input) {
  var ul = input.parentNode.querySelector('ul');
  var li = ul.querySelectorAll('li');
  var filter = input.value.toUpperCase();

  for (i = 0; i < li.length; i++) {
    var el = li[i];
    var item = el;
    txtValue = item.textContent || item.innerText;

    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      el.style.display = "";
    } else {
      el.style.display = "none";
    }
  }
} //DigiFormat


function digi(str) {
  var r = str.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
  return r;
} //Popups


var popup_link = document.querySelectorAll('._popup-link');

if (popup_link) {
  (function () {
    var body = document.querySelector("body");

    var _loop2 = function _loop2(_index2) {
      var el = popup_link[_index2];
      var item = el.getAttribute('href').replace('#', '');
      el.addEventListener('click', function (e) {
        if (!body.classList.contains('_wait')) {
          popup_open(item, '');
        }

        e.preventDefault;
      });
    };

    for (var _index2 = 0; _index2 < popup_link.length; _index2++) {
      _loop2(_index2);
    }
  })();
}

function popup_open(item) {
  var video = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var popup = document.querySelectorAll('.popup');

  if (popup) {
    //popup_close();
    history.pushState('', '', '#' + item);
    var curent_popup = document.querySelector('.popup_' + item);

    if (curent_popup) {
      if (video != '' && video != null) {
        var popup_video = document.querySelector('.popup_video');
        popup_video.querySelector('.popup__video').innerHTML = '<iframe src="https://www.youtube.com/embed/' + video + '?autoplay=1"  allow="autoplay; encrypted-media" allowfullscreen></iframe>';
      }

      if (!document.querySelector('.menu__body._active')) {
        body_lock_add(500);
      }

      curent_popup.classList.add('_active');
      curent_popup.addEventListener("click", function (e) {
        if (!e.target.closest('.popup__body')) {
          popup_close(e.target.closest('.popup'));
        }
      });
    }
  }
}

function popup_close(item) {
  var popup = document.querySelectorAll('.popup');
  var body = document.querySelector("body");

  if (!body.classList.contains('_wait')) {
    if (popup) {
      if (!item) {
        for (var _index3 = 0; _index3 < popup.length; _index3++) {
          var el = popup[_index3];
          el.classList.remove('_active');
        }
      } else {
        item.classList.remove('_active');
      }

      if (!document.querySelector('.menu__body._active')) {
        body_lock_remove(500);
      }

      history.pushState('', '', window.location.href.split('#')[0]);
    }
  }
}

var popup_close_icon = document.querySelectorAll('.popup__close');

if (popup_close_icon) {
  var _loop3 = function _loop3(_index4) {
    var el = popup_close_icon[_index4];
    el.addEventListener('click', function () {
      popup_close(el.closest('.popup'));
    });
  };

  for (var _index4 = 0; _index4 < popup_close_icon.length; _index4++) {
    _loop3(_index4);
  }
}

document.addEventListener('keydown', function (e) {
  if (e.which == 27) {
    popup_close();
  }
}); //Wrap

function wrap(el, wrapper) {
  el.parentNode.insertBefore(wrapper, el);
  wrapper.appendChild(el);
} //Placeholers


var inputs = document.querySelectorAll('input[data-value],textarea[data-value]');

if (inputs) {
  var _loop4 = function _loop4(_index5) {
    var input = inputs[_index5];
    var input_g_value = input.getAttribute('data-value');

    if (input.value == '' && input_g_value != '') {
      input.value = input_g_value;
    }

    if (input.value != '' && input.value != input_g_value) {
      input_focus_add(input);
    }

    input.addEventListener('focus', function (e) {
      if (input.value == input_g_value) {
        input_focus_add(input);
        input.value = '';
      }

      if (input.classList.contains('_phone')) {
        //'+7(999) 999 9999'
        //'+38(999) 999 9999'
        //'+375(99)999-99-99'
        Inputmask("+7(999) 999 9999", {
          //"placeholder": '',
          clearIncomplete: true,
          clearMaskOnLostFocus: true,
          onincomplete: function onincomplete() {
            input.inputmask.remove();
            input.value = input_g_value;
            input_focus_remove(input);
          }
        }).mask(input);
      }

      form_remove_error(input);
    });
    input.addEventListener('blur', function (e) {
      if (input.value == '') {
        if (input.classList.contains('_phone')) {
          if (input.inputmask) {
            input.inputmask.remove();
          }
        }

        input.value = input_g_value;
        input_focus_remove(input);
      }
    });

    if (input.classList.contains('_date')) {
      var datepicker = new Datepicker(input, {
        language: 'ru',
        autohide: true,
        showDaysOfWeek: false,
        format: 'dd-mm-yyyy'
      });
    }
  };

  for (var _index5 = 0; _index5 < inputs.length; _index5++) {
    _loop4(_index5);
  }
}

function input_focus_add(input) {
  input.classList.add('focus');
  input.parentElement.classList.add('focus');
}

function input_focus_remove(input) {
  input.classList.remove('focus');
  input.parentElement.classList.remove('focus');
}

var btn = document.querySelectorAll('button[type="submit"],input[type="submit"]');

if (btn) {
  for (var _index6 = 0; _index6 < btn.length; _index6++) {
    var el = btn[_index6];
    el.addEventListener('click', form_submit);
  }
}

function form_submit() {
  var error = 0;
  var btn = event.target;
  var form = btn.closest('form');
  var form_req = form.querySelectorAll('._req');

  if (form_req) {
    for (var _index7 = 0; _index7 < form_req.length; _index7++) {
      var _el = form_req[_index7];
      error += form_validate(_el);
    }
  }

  if (error == 0) {
    //SendForm
    form_send(form); //popup_close();
    //popup_open('message');

    event.preventDefault();
  } else {
    var form_error = form.querySelectorAll('._error');

    if (form_error && form.classList.contains('_goto-error')) {
      _goto(form_error[0], 1000, 50);
    }

    event.preventDefault();
  }
}

function form_validate(input) {
  var error = 0;
  var input_g_value = input.getAttribute('data-value');

  if (input.getAttribute("name") == "email" || input.classList.contains("_email")) {
    if (input.value != input_g_value) {
      var em = input.value.replace(" ", "");
      input.value = em;
    }

    if (email_test(input) || input.value == input_g_value) {
      form_add_error(input);
      error++;
    } else {
      form_remove_error(input);
    }
  } else {
    if (input.value == '' || input.value == input_g_value) {
      form_add_error(input);
      error++;
    } else {
      form_remove_error(input);
    }
  }

  return error;
}

function form_clear(form) {
  var inputs = form.querySelectorAll('input,textarea');

  for (var _index8 = 0; _index8 < inputs.length; _index8++) {
    var input = inputs[_index8];
    input.value = '';
    input.value = input.getAttribute('data-value');
  }
}

function form_add_error(input) {
  input.classList.add('_error');
  input.parentElement.classList.add('_error');
  var input_error = input.parentElement.querySelector('.form__error');

  if (input_error) {
    input.parentElement.removeChild(input_error);
  }

  var input_error_text = input.getAttribute('data-error');

  if (input_error_text && input_error_text != '') {
    input.parentElement.insertAdjacentHTML('beforeend', '<div class="form__error">' + input_error_text + '</div>');
  }
}

function form_remove_error(input) {
  input.classList.remove('_error');
  input.parentElement.classList.remove('_error');
  var input_error = input.parentElement.querySelector('.form__error');

  if (input_error) {
    input.parentElement.removeChild(input_error);
  }
} //Select


function selects_init() {
  var selects = document.querySelectorAll('select');

  if (selects) {
    for (var _index9 = 0; _index9 < selects.length; _index9++) {
      var select = selects[_index9];
      select_init(select);
    } //select_callback();

  }
}

function select_init(select) {
  var select_parent = select.parentElement;
  var select_modifikator = select.getAttribute('class');
  select.style.display = 'none';
  select_parent.insertAdjacentHTML('beforeend', '<div class="select select_' + select_modifikator + '"></div>');
  var new_select = select.parentElement.querySelector('.select');
  new_select.append(select);
  select_item(select);
}

function select_item(select) {
  var select_parent = select.parentElement;
  var select_items = select_parent.querySelector('.select__item');
  var select_options = select.querySelectorAll('option');
  var select_selected_option = select.querySelector('option:checked');
  var select_selected_text = select_selected_option.text;
  var select_type = select.getAttribute('data-type');

  if (select_items) {
    select_items.remove();
  }

  var select_type_content = '';

  if (select_type == 'input') {
    select_type_content = '<div class="select__value icon-select-arrow"><input autocomplete="off" type="text" name="form[]" value="' + select_selected_text + '" data-error="Ошибка" data-value="' + select_selected_text + '" class="select__input"></div>';
  } else {
    select_type_content = '<div class="select__value icon-select-arrow">' + select_selected_text + '</div>';
  }

  select_parent.insertAdjacentHTML('beforeend', '<div class="select__item">' + '<div class="select__title">' + select_type_content + '</div>' + '<div class="select__options">' + select_get_options(select_options) + '</div>' + '</div></div>');
  select_actions(select, select_parent);
}

function select_actions(original, select) {
  var select_item = select.querySelector('.select__item');
  var select_options = select.querySelectorAll('.select__option');
  var select_type = original.getAttribute('data-type');
  var select_input = select.querySelector('.select__input');
  select_item.addEventListener('click', function () {
    select.classList.toggle('_active');
  });

  var _loop5 = function _loop5(_index10) {
    var select_option = select_options[_index10];
    var select_option_value = select_option.getAttribute('data-value');
    var select_option_text = select_option.innerHTML;

    if (select_type != 'input') {
      if (select_option.getAttribute('data-value') == original.value) {
        select_option.style.display = 'none';
      }
    }

    select_option.addEventListener('click', function () {
      for (var _index11 = 0; _index11 < select_options.length; _index11++) {
        var _el2 = select_options[_index11];
        _el2.style.display = 'block';
      }

      if (select_type == 'input') {
        select_input.value = select_option_text;
        original.value = select_option_value;
      } else {
        select.querySelector('.select__value').innerHTML = select_option_text;
        original.value = select_option_value;
        select_option.style.display = 'none';
      }
    });
  };

  for (var _index10 = 0; _index10 < select_options.length; _index10++) {
    _loop5(_index10);
  }
}

function select_get_options(select_options) {
  if (select_options) {
    var select_options_content = '';

    for (var _index12 = 0; _index12 < select_options.length; _index12++) {
      var select_option = select_options[_index12];
      var select_option_value = select_option.value;

      if (select_option_value != '') {
        var select_option_text = select_option.text;
        select_options_content = select_options_content + '<div data-value="' + select_option_value + '" class="select__option">' + select_option_text + '</div>';
      }
    }

    return select_options_content;
  }
}

function selects_update_all() {
  var selects = document.querySelectorAll('select');

  if (selects) {
    for (var _index13 = 0; _index13 < selects.length; _index13++) {
      var select = selects[_index13];
      select_item(select);
    }
  }
}

selects_init(); //BildSlider

var sliders = document.querySelectorAll('._swiper');

if (sliders) {
  for (var _index14 = 0; _index14 < sliders.length; _index14++) {
    var slider = sliders[_index14];

    if (!slider.classList.contains('swiper-bild')) {
      var slider_items = slider.children;

      if (slider_items) {
        for (var _index15 = 0; _index15 < slider_items.length; _index15++) {
          var _el3 = slider_items[_index15];

          _el3.classList.add('swiper-slide');
        }
      }

      var slider_content = slider.innerHTML;
      var slider_wrapper = document.createElement('div');
      slider_wrapper.classList.add('swiper-wrapper');
      slider_wrapper.innerHTML = slider_content;
      slider.innerHTML = ''; //slider.insertAdjacentHTML('beforeend', slider_wrapper);

      slider.appendChild(slider_wrapper);
      slider.classList.add('swiper-bild');
    }

    if (slider.classList.contains('_gallery')) {//slider.data('lightGallery').destroy(true);
    }
  }

  sliders_bild_callback();
}

function sliders_bild_callback(params) {}

var slider_about = new Swiper('.main-slider', {
  observer: true,
  observeParents: true,
  slidesPerView: 5,
  spaceBetween: 0,
  autoHeight: false,
  speed: 800,
  //touchRatio: 0,
  //simulateTouch: false,
  //loop: true,
  //preloadImages: false,
  //lazy: true,
  // Dotts,
  pagination: {
    el: '.block-page__dotts',
    clickable: true
  },
  // Arrows
  navigation: {
    nextEl: '.block-page__arrow_next',
    prevEl: '.block-page__arrow_prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 0
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 0
    },
    992: {
      slidesPerView: 3,
      spaceBetween: 0
    },
    1140: {
      slidesPerView: 4,
      spaceBetween: 0
    },
    1500: {
      slidesPerView: 5,
      spaceBetween: 0
    }
  },
  on: {
    lazyImageReady: function lazyImageReady() {
      ibg();
    }
  } // And if we need scrollbar
  //scrollbar: {
  //	el: '.swiper-scrollbar',
  //},

}); // Dynamic Adapt v.1
// HTML data-move="where(uniq class name),position(digi),when(breakpoint)"
// e.x. data-move="item,2,992"
// Andrikanych Yevhen 2020

var move_array = [];
var move_objects = document.querySelectorAll("[data-move]");

if (move_objects.length > 0) {
  for (var _index16 = 0; _index16 < move_objects.length; _index16++) {
    var _el4 = move_objects[_index16];

    var data_move = _el4.getAttribute("data-move");

    if (data_move != "" || data_move != null) {
      _el4.setAttribute("data-move-index", _index16);

      move_array[_index16] = {
        parent: _el4.parentNode,
        index: index_in_parent(_el4)
      };
    }
  }
}

function dynamic_adapt() {
  var w = document.querySelector("body").offsetWidth;

  if (move_objects.length > 0) {
    for (var _index17 = 0; _index17 < move_objects.length; _index17++) {
      var _el5 = move_objects[_index17];

      var _data_move = _el5.getAttribute("data-move");

      if (_data_move != "" || _data_move != null) {
        var data_array = _data_move.split(",");

        var data_parent = document.querySelector("." + data_array[0]);
        var data_index = data_array[1];
        var data_bp = data_array[2];

        if (w < data_bp) {
          if (!_el5.classList.contains("js-move_done_" + data_bp)) {
            if (data_index > 0) {
              //insertAfter
              var actual_index = index_of_elements(data_parent)[data_index];
              data_parent.insertBefore(_el5, data_parent.childNodes[actual_index]);
            } else {
              data_parent.insertBefore(_el5, data_parent.firstChild);
            }

            _el5.classList.add("js-move_done_" + data_bp);
          }
        } else {
          if (_el5.classList.contains("js-move_done_" + data_bp)) {
            dynamic_adaptive_back(_el5);

            _el5.classList.remove("js-move_done_" + data_bp);
          }
        }
      }
    }
  }

  custom_adapt(w);
}

function dynamic_adaptive_back(el) {
  var index_original = el.getAttribute("data-move-index");
  var move_place = move_array[index_original];
  var parent_place = move_place["parent"];
  var index_place = move_place["index"];

  if (index_place > 0) {
    //insertAfter
    var actual_index = index_of_elements(parent_place)[index_place];
    parent_place.insertBefore(el, parent_place.childNodes[actual_index]);
  } else {
    parent_place.insertBefore(el, parent_place.firstChild);
  }
}

function index_in_parent(node) {
  var children = node.parentNode.childNodes;
  var num = 0;

  for (var _i = 0; _i < children.length; _i++) {
    if (children[_i] == node) return num;
    if (children[_i].nodeType == 1) num++;
  }

  return -1;
}

function index_of_elements(parent) {
  var children = [];

  for (var _i2 = 0; _i2 < parent.childNodes.length; _i2++) {
    if (parent.childNodes[_i2].nodeType == 1 && parent.childNodes[_i2].getAttribute("data-move") == null) {
      children.push(_i2);
    }
  }

  return children;
}

window.addEventListener("resize", function (event) {
  dynamic_adapt();
});
dynamic_adapt();

function custom_adapt(w) {}

var scr_body = document.querySelector('body');
var scr_blocks = document.querySelectorAll('._scr-sector');
var scr_items = document.querySelectorAll('._scr-item');
var scr_min_height = 750; //ScrollOnScroll

window.addEventListener('scroll', scroll_scroll);

function scroll_scroll() {
  scr_body.setAttribute('data-scroll', pageYOffset);

  if (scr_blocks.length > 0) {
    for (var _index18 = 0; _index18 < scr_blocks.length; _index18++) {
      var block = scr_blocks[_index18];
      var block_offset = offset(block).top;
      var block_height = block.offsetHeight;

      if (pageYOffset > block_offset - window.innerHeight / 1.5 && pageYOffset < block_offset + block_height - window.innerHeight / 1.5) {
        block.classList.add('_scr-sector_active');
      } else {
        if (block.classList.contains('_scr-sector_active')) {
          block.classList.remove('_scr-sector_active');
        }
      }

      if (pageYOffset > block_offset - window.innerHeight / 2 && pageYOffset < block_offset + block_height - window.innerHeight / 2) {
        if (!block.classList.contains('_scr-sector_current')) {
          block.classList.add('_scr-sector_current');
        }
      } else {
        if (block.classList.contains('_scr-sector_current')) {
          block.classList.remove('_scr-sector_current');
        }
      }
    }
  }

  if (scr_items.length > 0) {
    for (var _index19 = 0; _index19 < scr_items.length; _index19++) {
      var scr_item = scr_items[_index19];
      var scr_item_offset = offset(scr_item).top;
      var scr_item_height = scr_item.offsetHeight;

      if (pageYOffset > scr_item_offset - window.innerHeight / 1.5 && pageYOffset < scr_item_offset + scr_item_height - window.innerHeight / 1.5) {
        scr_item.classList.add('_active');
      } else {
        scr_item.classList.remove('_active');
      }
    }
  }
}

scroll_scroll(); //FullScreenScroll

if (scr_blocks.length > 0 && !isMobile.any()) {
  disableScroll();
  window.addEventListener('wheel', full_scroll);
}

function full_scroll(e) {
  var viewport_height = window.innerHeight;

  if (viewport_height >= scr_min_height) {
    if (!scr_body.classList.contains('_scrolling')) {
      // ВЫЧИСЛИТЬ!!!
      var current_scroll = parseInt(scr_body.getAttribute('data-scroll')); //

      var current_block = document.querySelector('._scr-sector._scr-sector_current');
      var current_block_pos = offset(current_block).top;
      var current_block_height = current_block.offsetHeight;
      var current_block_next = current_block.nextElementSibling;
      var current_block_prev = current_block.previousElementSibling;
      var block_pos;

      if (e.keyCode == 40 || e.keyCode == 34 || e.deltaX > 0 || e.deltaY < 0) {
        if (current_block_prev) {
          var current_block_prev_height = current_block_prev.offsetHeight;
          block_pos = offset(current_block_prev).top;

          if (current_block_height <= viewport_height) {
            if (current_block_prev_height >= viewport_height) {
              block_pos = block_pos + (current_block_prev_height - viewport_height);
              full_scroll_to_sector(block_pos);
            }
          } else {
            enableScroll();

            if (current_scroll <= current_block_pos) {
              full_scroll_to_sector(block_pos);
            }
          }
        } else {
          full_scroll_pagestart();
        }
      } else if (e.keyCode == 38 || e.keyCode == 33 || e.deltaX < 0 || e.deltaY > 0) {
        if (current_block_next) {
          block_pos = offset(current_block_next).top;

          if (current_block_height <= viewport_height) {
            full_scroll_to_sector(block_pos);
          } else {
            enableScroll();

            if (current_scroll >= block_pos - viewport_height) {
              full_scroll_to_sector(block_pos);
            }
          }
        } else {
          full_scroll_pageend();
        }
      }
    } else {
      disableScroll();
    }
  } else {
    enableScroll();
  }
}

function full_scroll_to_sector(pos) {
  disableScroll();
  scr_body.classList.add('_scrolling');

  _goto(pos, 800);

  var scr_pause = 500;

  if (navigator.appVersion.indexOf("Mac") != -1) {
    scr_pause = 1000;
  }

  ;
  setTimeout(function () {
    scr_body.classList.remove('_scrolling');
  }, scr_pause);
}

function full_scroll_pagestart() {}

function full_scroll_pageend() {} //ScrollOnClick (Navigation)


var link = document.querySelectorAll('._goto-block');

if (link) {
  var blocks = [];

  var _loop6 = function _loop6(_index20) {
    var el = link[_index20];
    var block_name = el.getAttribute('href').replace('#', '');

    if (block_name != '' && !~blocks.indexOf(block_name)) {
      blocks.push(block_name);
    }

    el.addEventListener('click', function (e) {
      if (document.querySelector('.menu__body._active')) {
        menu_close();
        body_lock_remove(500);
      }

      var target_block_class = el.getAttribute('href').replace('#', '');
      var target_block = document.querySelector('.' + target_block_class);

      _goto(target_block, 300);

      e.preventDefault();
    });
  };

  for (var _index20 = 0; _index20 < link.length; _index20++) {
    _loop6(_index20);
  }

  window.addEventListener('scroll', function (el) {
    var old_current_link = document.querySelectorAll('._goto-block._active');

    if (old_current_link) {
      for (var _index21 = 0; _index21 < old_current_link.length; _index21++) {
        var _el6 = old_current_link[_index21];

        _el6.classList.remove('_active');
      }
    }

    for (var _index22 = 0; _index22 < blocks.length; _index22++) {
      var block = blocks[_index22];
      var block_item = document.querySelector('.' + block);

      if (block_item) {
        var block_offset = offset(block_item).top;
        var block_height = block_item.offsetHeight;

        if (pageYOffset > block_offset - window.innerHeight / 3 && pageYOffset < block_offset + block_height - window.innerHeight / 3) {
          var current_links = document.querySelectorAll('._goto-block[href="#' + block + '"]');

          for (var _index23 = 0; _index23 < current_links.length; _index23++) {
            var current_link = current_links[_index23];
            current_link.classList.add('_active');
          }
        }
      }
    }
  });
} //ScrollOnClick (Simple)


var goto_links = document.querySelectorAll('._goto');

if (goto_links) {
  var _loop7 = function _loop7(_index24) {
    var goto_link = goto_links[_index24];
    goto_link.addEventListener('click', function (e) {
      var target_block_class = goto_link.getAttribute('href').replace('#', '');
      var target_block = document.querySelector('.' + target_block_class);

      _goto(target_block, 300);

      e.preventDefault();
    });
  };

  for (var _index24 = 0; _index24 < goto_links.length; _index24++) {
    _loop7(_index24);
  }
}

function _goto(target_block, speed) {
  var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var header = ''; //OffsetHeader
  //if (window.innerWidth < 992) {

  header = 'header'; //}

  var options = {
    speed: speed,
    header: header,
    offset: offset
  };
  var scr = new SmoothScroll();
  scr.animateScroll(target_block, '', options);
} //SameFunctions


function offset(el) {
  var rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return {
    top: rect.top + scrollTop,
    left: rect.left + scrollLeft
  };
}

function disableScroll() {
  if (window.addEventListener) // older FF
    window.addEventListener('DOMMouseScroll', preventDefault, false);
  document.addEventListener('wheel', preventDefault, {
    passive: false
  }); // Disable scrolling in Chrome

  window.onwheel = preventDefault; // modern standard

  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE

  window.ontouchmove = preventDefault; // mobile

  document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
  if (window.removeEventListener) window.removeEventListener('DOMMouseScroll', preventDefault, false);
  document.removeEventListener('wheel', preventDefault, {
    passive: false
  }); // Enable scrolling in Chrome

  window.onmousewheel = document.onmousewheel = null;
  window.onwheel = null;
  window.ontouchmove = null;
  document.onkeydown = null;
}

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault) e.preventDefault();
  e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
  /*if (keys[e.keyCode]) {
  	preventDefault(e);
  	return false;
  }*/
}

function map(n) {
  google.maps.Map.prototype.setCenterWithOffset = function (latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();

    ov.onAdd = function () {
      var proj = this.getProjection();
      var aPoint = proj.fromLatLngToContainerPixel(latlng);
      aPoint.x = aPoint.x + offsetX;
      aPoint.y = aPoint.y + offsetY;
      map.panTo(proj.fromContainerPixelToLatLng(aPoint)); //map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
    };

    ov.draw = function () {};

    ov.setMap(this);
  };

  var markers = new Array();
  var infowindow = new google.maps.InfoWindow({//pixelOffset: new google.maps.Size(-230,250)
  });
  var locations = [[new google.maps.LatLng(59.9215202, 30.4132455)]];
  var options = {
    zoom: 17,
    panControl: false,
    mapTypeControl: false,
    center: locations[0][0],
    styles: [{
      "featureType": "all",
      "stylers": [{
        "saturation": 0
      }, {
        "hue": "#e7ecf0"
      }]
    }, {
      "featureType": "road",
      "stylers": [{
        "saturation": -70
      }]
    }, {
      "featureType": "transit",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "poi",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "water",
      "stylers": [{
        "visibility": "simplified"
      }, {
        "saturation": -60
      }]
    }],
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById('map'), options);
  var icon = {
    url: '/wp-content/themes/willant/img/icons/location.png',
    scaledSize: new google.maps.Size(55, 71),
    anchor: new google.maps.Point(22, 35)
  };

  for (var i = 0; i < locations.length; i++) {
    var marker = new google.maps.Marker({
      icon: icon,
      position: locations[i][0],
      map: map
    });
    google.maps.event.addListener(marker, 'click', function (marker, i) {
      return function () {
        for (var m = 0; m < markers.length; m++) {
          markers[m].setIcon(icon);
        }

        var cnt = i + 1;
        infowindow.setContent('');
        infowindow.open(map, marker);
        marker.setIcon(icon);
        map.setCenterWithOffset(marker.getPosition(), 0, 0);
        setTimeout(function () {//baloonstyle();
        }, 10);
      };
    }(marker, i));
    markers.push(marker);
  }

  if (n) {
    var nc = n - 1;
    setTimeout(function () {
      google.maps.event.trigger(markers[nc], 'click');
    }, 500);
  }
}

function baloonstyle() {
  $('.gm-style-iw').parent().addClass('baloon');
  $('.gm-style-iw').prev().addClass('baloon-style');
  $('.gm-style-iw').next().addClass('baloon-close');
  $('.gm-style-iw').addClass('baloon-content');
}

var m = document.getElementById('map');

if (m) {
  map(1);
}
/* YA
function map(n){
	ymaps.ready(init);
	function init(){
		// Создание карты.
		var myMap = new ymaps.Map("map", {
			// Координаты центра карты.
			// Порядок по умолчанию: «широта, долгота».
			// Чтобы не определять координаты центра карты вручную,
			// воспользуйтесь инструментом Определение координат.
			controls: [],
			center: [43.585525,39.723062],
			// Уровень масштабирования. Допустимые значения:
			// от 0 (весь мир) до 19.
			zoom: 10
		});

		myPlacemar = new ymaps.Placemark([43.585525,39.723062],{
			id:'2'
		},{
			// Опции.
			hasBalloon:false,
			hideIconOnBalloonOpen:false,
			// Необходимо указать данный тип макета.
			iconLayout: 'default#imageWithContent',
			// Своё изображение иконки метки.
			iconImageHref: 'img/icons/map.svg',
			// Размеры метки.
			iconImageSize: [40, 40],
			// Смещение левого верхнего угла иконки относительно
			// её "ножки" (точки привязки).
			iconImageOffset: [-20, -20],
			// Смещение слоя с содержимым относительно слоя с картинкой.
			iconContentOffset: [0,0],
		});
		myMap.geoObjects.add(myPlacemar);

		myMap.behaviors.disable('scrollZoom');
	}
}
*/


var searh_icon = document.querySelector(".item-body-header__search");
searh_icon.addEventListener("click", function (e) {
  document.querySelector('.item-body-header__form').classList.add("_active");
  e.preventDefault();
});
var serach_close = document.querySelector(".form-block__closed");
serach_close.addEventListener("click", function (e) {
  document.querySelector('.item-body-header__form').classList.remove("_active");
  e.preventDefault();
});
var menu_parent = document.querySelector('.menu__parent');

if (window.innerWidth > 1160) {
  menu_parent.addEventListener("mouseenter", function (e) {
    menu_parent.classList.add('hover');
  });
  menu_parent.addEventListener("mouseleave", function (e) {
    menu_parent.classList.remove('hover');
  });
} else {
  var link_parent = document.querySelector('.menu__link_parent');
  link_parent.addEventListener("click", function (e) {
    menu_parent.classList.toggle('active');
    e.preventDefault();
  });
}

var tables = document.querySelectorAll('.content table');

if (tables.length > 0) {
  for (var _index25 = 0; _index25 < tables.length; _index25++) {
    var table = tables[_index25];
    var table_wrapper = document.createElement('div');
    table_wrapper.classList.add('tables__block');
    wrap(table, table_wrapper);
    var th = table.querySelectorAll('th');

    for (var _index26 = 0; _index26 < th.length; _index26++) {
      var _el7 = th[_index26];

      if (!_el7.querySelector('span')) {
        var span = document.createElement('span');
        span.innerHTML = _el7.innerHTML;
        _el7.innerHTML = '';

        _el7.appendChild(span);
      }
    }

    var td = table.querySelectorAll('td');

    for (var _index27 = 0; _index27 < td.length; _index27++) {
      var _el8 = td[_index27];

      if (!_el8.querySelector('span')) {
        var _span = document.createElement('span');

        _span.innerHTML = _el8.innerHTML;
        _el8.innerHTML = '';

        _el8.appendChild(_span);
      }
    }
  }
}

var link_goto = document.querySelectorAll('.uagb-toc__list a');

if (link_goto.length > 0) {
  var _loop8 = function _loop8(_index28) {
    var el = link_goto[_index28];
    el.addEventListener("click", function (e) {
      var obj_name = el.getAttribute('href').replace('#', '');
      var obj = document.getElementById(obj_name); //console.log(obj);

      _goto(obj, 300);

      e.preventDefault();
    });
  };

  for (var _index28 = 0; _index28 < link_goto.length; _index28++) {
    _loop8(_index28);
  }
}